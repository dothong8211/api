<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\PostController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware(['api'])->group(function () {
//     Route::prefix('v1')->group(function (){
//         Route::get('/', [PostController::class, 'index']);
//     });
// });

Route::apiResource('posts', PostController::class);


