<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Http\Resources\PostCollection;
use App\Http\Resources\PostResource;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use Illuminate\Http\Response;


class PostController extends Controller
{
    protected $post;

    public function __construct(Post $post)
    {
        $this->post = $post;
    }

    public function index()
    {
        $posts = $this->post->paginate(10);
        $postCollection = new PostCollection($posts);
        return $this->responseData($postCollection, Response::HTTP_OK, 'succsess');
    }

    public function store(StorePostRequest $request)
    {
        $data = $request->all();
        $post = $this->post->create($data);
        $PostResource = new PostResource($post);
        return $this->responseData($PostResource, Response::HTTP_OK, 'succsess');
    }

    public function show($id)
    {
        $post = $this->post->findOrFail($id);
        $postResource = new PostResource($post);
        return $this->responseData($postResource, Response::HTTP_OK, 'succsess');
    }

    public function update(UpdatePostRequest $request, $id)
    {
        $data = $request->all();
        $post = $this->post->findOrFail($id);
        $post->update($data);
        $postResource = new postResource($post);
        return $this->responseData($postResource, Response::HTTP_OK, 'succsess');
    }

    public function destroy($id)
    {
        $post = $this->post->findOrFail($id);
        $post->delete();
        $postResource = new PostResource($post);
        return $this->responseData($postResource, Response::HTTP_OK, 'succsess');
    }
}
