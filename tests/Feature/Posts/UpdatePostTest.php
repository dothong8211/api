<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class UpdatePostTest extends TestCase
{
    use WithFaker;

    /** @test*/
    public function user_can_update_post()
    {
        $post = Post::factory()->create();
        $data = [
            'title' => $this->faker->title,
            'description' =>$this->faker->text
        ];

        $response = $this->json('PUT', route('post.update', $post->id), $data);
        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(function (AssertableJson $json) use ($data){
            $json->has('data', function (AssertableJson $json) use ($data){
                    $json->where('title', $data['title'])
                         ->where('description', $data['description'])
                         ->etc();
            });
        });
    }

    /** @test */
    public function user_can_not_update_post_if_post_not_exists(){
        $postId = -1;
        $data = [
            'title' => 'thong',
            'description'=> 'nhuế, kim chung đông anh'
        ];
        $response = $this->json('PUT', route('post.update', $postId), $data);
        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }

    /** @test*/
    public function user_can_not_update_post_if_data_invalid(){
        $post = Post::factory()->create();
        $data = [
            'title' => $this->faker->title,
            'description'=> ''
        ];

        $response = $this->json('PUT', route('post.update', $post->id), $data);

        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(function(AssertableJson $json) use ($data){
            $json->has('error', function (AssertableJson $json){
                $json->has('description');
            });
        });
    }
}
