<?php

namespace Tests\Feature\Posts;

use Facade\FlareClient\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class getListPostTest extends TestCase
{
    /** @test */
    public function user_can_get_list_post()
    {
        $response = $this->getJson(route('post.index'));

        $response->assertStatus(200);
        $response->assertJson(function (AssertableJson $json){
            $json->has('data')
                ->etc();
        });
    }
}
