<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class DeletePostTest extends TestCase
{
    /** @test*/
    public function user_can_delete_post_if_post_exists(){
        $post = Post::factory()->create();
        $response = $this->json('DELETE', route('post.destroy', $post->id));
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(function (AssertableJson $json) use ($post){
            $json->has('data', function (AssertableJson $json) use ($post){
                $json->where('title', $post->title)
                     ->etc();
            })
                ->has('message');
        });
    }

    /** @test*/
    public function user_can_not_delete_post_if_not_exists(){
        $postId = -1;
        $response = $this->json('DELETE', route('post.destroy', $postId));
        $response->assertStatus(Response::HTTP_NOT_FOUND);
        $response->assertJson(function (AssertableJson $json){
            $json->hasAll('message', 'status');
        });
    }
}
