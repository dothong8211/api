<?php

namespace Tests\Feature\Posts;

use Illuminate\Http\Response;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class CreatePostTest extends TestCase
{
    /** @test*/
    use WithFaker;
    public function user_can_create_post(){
        $data = [
            'title' => $this->faker->name,
            'description' => $this->faker->text
        ];

        $response = $this->json('POST', route('post.store'), $data);
        $response->assertStatus(Response::HTTP_OK);
        $response->assertJson(function (AssertableJson $json) use ($data) {
            $json->has('data', function (AssertableJson $json) use ($data) {
                $json->where('title', $data['title'])
                ->etc();
            });
        });

        $this->assertDatabaseHas('post', [
            'title' => $data['title'],
            'description' => $data['description']
        ]);
    }

    /** @test*/
    public function user_can_not_create_if_description_empty(){
        $data =[
            'title'=> $this->faker->name,
            'description' => ''
        ];

        $response = $this->json('POST',route('post.store'), $data);
        $response->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
        $response->assertJson(function (AssertableJson $json){
            $json->has('error', function (AssertableJson $json){
                $json->has('description');
            });
        });
    }
}
