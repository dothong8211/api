<?php

namespace Tests\Feature\Posts;

use App\Models\Post;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class getPostTest extends TestCase
{
    /** @test */
    public function user_can_get_post_if_post_exists()
    {
        $post = Post::factory()->create();

        $response = $this->getJson(route('post.show', $post->id));

        $response->assertStatus(Response::HTTP_OK);

        $response->assertJson(function (AssertableJson $json) use ($post) {
            return $json->has('data', function (AssertableJson $json) use ($post) {
                $json->where('id', $post->id)
                    ->etc();
            });
        });
    }

    /** @test */
    public function user_can_not_get_post_if_post_exists()
    {
        $postId = -1;
        $response = $this->getJson(route('post.show', $postId));

        $response->assertStatus(Response::HTTP_NOT_FOUND);
    }
}
