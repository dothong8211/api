<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Post;
use Illuminate\Foundation\Testing\WithFaker;

class PostFactory extends Factory
{
    protected $model = Post::class;

    use WithFaker;

    public function definition()
    {
        return [
            'title' => $this->faker->name,
            'description' => $this->faker->text
        ];
    }
}
